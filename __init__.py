#!/usr/bin/env python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import gui

win=gui.MainWindow()
win.connect("delete-event", Gtk.main_quit)
Gtk.main()