Midgard
=======

Midgard is a file manager. The name is a pun relating the Midgard Serpent of norse mythology to the python programming language it's written in, as well as an homage to the XFCE file manager, Thunar, which is also named after norse mythology.

The old (Windows 95, extremely old Nautilus) GUI file manager metaphor was to use one window per folder. This was good for dragging files around and spatial navigation, but was cumbersome to manage. Then we switched to one window in a browser type mode. This was much easier to manage, but had no spatial navigation and made dragging files around more difficult, necessitating aggressive use of the back/forward/up buttons. Numerous UI components were tacked on to try to add these abilities back to browsers without making the window management complex, such as breadcrumbs and tabs, and the re-popularization of the even older two-pane file manager layout.

Midgard tries to be the antithesis of the old multi-window metaphor while still presenting multiple folders as discrete visual units. It does this using miller columns, a feature supported by some modern file managers like Finder and Pantheon Files, to display the contents of multiple folders at once. Unlike Finder and Pantheon, it allows opening multiple "chains" of folders side-by-side.

Planned features
----------------

* statusbar/bottom info pane
* single instance mode
* standard keyboard shortcuts
* tightly integrated terminal
* separate indicator of which folder is open in the next column
* better selection checkbox behavior
* drag and drop
* right click menu

Midgard does not go out of its way to provide many display options to the user, unlike most other file managers. There will never be a feature, for example, to add back/forward buttons to the application, as these tasks are intended to be better served by the miller column format. If you want a heavily customizable file manager (and I don't blame you; I did spend years of my life trying to find one I liked before writing Midgard), it's best to look elsewhere.