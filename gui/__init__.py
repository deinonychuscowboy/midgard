from typing import Optional

import gi
import data

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk,GObject,Pango,Gdk

SINGLE_CLICK=True
DEFAULT_ZOOM=200

NAME_DATA=0

SELECT_COL=0
ICON_COL=1
NAME_COL=2
class DirectoryView(Gtk.Bin):
	def __init__(self,chain:'DirectoryChain',dir:data.Directory,zoom:int):
		Gtk.Bin.__init__(self)

		self.directory=dir
		self.chain=chain

		self._last_selected=[]
		self._selected=[]

		self._hbox=Gtk.HBox()
		self._frame=Gtk.EventBox()
		self._scroller=Gtk.ScrolledWindow()
		self._scroller.set_policy(1,1)
		self._frame.add(self._scroller)
		self._hbox.pack_start(self._frame,True,True,0)
		self._separator=Gtk.VSeparator()
		self._hbox.pack_start(self._separator,False,False,2.5)
		self.add(self._hbox)
		self._treeview=Gtk.TreeView()
		self._scroller.add(self._treeview)

		self._scroller.props.border_width=1

		self._treeview.get_selection().props.mode=Gtk.SelectionMode.MULTIPLE
		self._treeview.props.activate_on_single_click=SINGLE_CLICK
		self._treeview.props.show_expanders=False

		if SINGLE_CLICK:
			renderer_select=Gtk.CellRendererToggle()
			self._column_select=Gtk.TreeViewColumn('',renderer_select)
			self._column_select.props.sizing=Gtk.TreeViewColumnSizing.FIXED
			self._column_select.props.resizable=False
			self._column_select.props.max_width=22
			self._column_select.props.min_width=22
			self._column_select.set_cell_data_func(renderer_select,self._render_select)
			self._treeview.append_column(self._column_select)

		renderer_icon=Gtk.CellRendererPixbuf()
		self._column_icon=Gtk.TreeViewColumn('',renderer_icon)
		self._column_icon.props.sizing=Gtk.TreeViewColumnSizing.FIXED
		self._column_icon.props.resizable=False
		self._column_icon.set_cell_data_func(renderer_icon,self._render_icon)
		self._treeview.append_column(self._column_icon)

		renderer_name=Gtk.CellRendererText()
		renderer_name.set_property("ellipsize",Pango.EllipsizeMode.END)
		self._column_name=Gtk.TreeViewColumn("Name", renderer_name)
		self._column_name.props.sizing=Gtk.TreeViewColumnSizing.AUTOSIZE
		self._column_name.set_cell_data_func(renderer_name,self._render_name)
		self._treeview.append_column(self._column_name)

		self._treeview.connect("row-activated",self._handle_row_activated)
		self._treeview.get_selection().connect("changed",self._handle_row_selected)

		self.populate()

		self._zoom=zoom
		self.zoom(zoom)

		color=self._treeview.get_style_context().get_background_color(Gtk.StateFlags(Gtk.StateType.NORMAL))
		self._scroller.modify_bg(Gtk.StateType.NORMAL,Gdk.Color(color.red*65535,color.green*65535,color.blue*65535))
		self.dismiss_focus()

		self._frame.connect("button-press-event",self._handle_button_press)
		self._treeview.connect("button-press-event",self._handle_button_press)

		self.show_all()

	def _determine_icon_size(self):
		icon_size=self._zoom/12.0
		if icon_size<22:
			icon_size=1
		elif icon_size<32:
			icon_size=3
		else:
			icon_size=5
		return icon_size

	def _translate_icon_size(self,size:int):
		if size==1:
			return 22
		elif size==3:
			return 32
		else:
			return 48

	def _translate_row_size(self,size:int):
		if size==1:
			return 22*0.9
		elif size==3:
			return 32*0.8
		else:
			return 48*0.7

	def zoom(self,zoom:int):
		self.props.width_request=zoom
		self._zoom=zoom
		icon_size=self._determine_icon_size()
		self._column_icon.props.min_width=self._translate_icon_size(icon_size)
		self._column_icon.props.max_width=self._translate_icon_size(icon_size)
		self.populate()

	def populate(self):
		self._treeview.set_model(Gtk.TreeStore(GObject.GType(data.Entry)))
		for entry in self.directory:
			if entry.hidden==False:
				self._treeview.get_model().append(None,(entry,))
		self._sync_selection()

	def deselect_all(self):
		self._last_selected=[]
		self._selected=[]
		self._sync_selection()

	def _indicate_focus(self):
		MainWindow.instance.set_focus(self)
		color=self._treeview.get_style_context().get_background_color(Gtk.StateFlags(Gtk.StateType.FOCUSED))
		self._frame.modify_bg(Gtk.StateType.NORMAL,Gdk.Color(color.red*65535,color.green*65535,color.blue*65535))

	def _sync_selection(self):
		real_selection=self._treeview.get_selection().get_selected_rows()[1]
		last_selected=self._last_selected
		for selected_path in last_selected:
			if selected_path not in real_selection:
				self._treeview.get_selection().select_path(selected_path)
		for selected_path in real_selection:
			if selected_path not in last_selected:
				self._treeview.get_selection().unselect_path(selected_path)
		self._last_selected=self._treeview.get_selection().get_selected_rows()[1]
		self._selected=self._last_selected

	def _handle_button_press(self,sender,event):
		self._indicate_focus()

	def _handle_row_activated(self,sender,path:Gtk.TreePath,column:Gtk.TreeViewColumn):
		if not SINGLE_CLICK or self._treeview.get_columns()[SELECT_COL] != column:
			treeiter=self._treeview.get_model().get_iter(path)
			entry=self._treeview.get_model().get_value(treeiter, NAME_DATA)
			if isinstance(entry,data.Directory):
				self.chain.open_directory(self.directory,entry)
			else:
				# open/execute
				pass
			self._last_selected=[]

		# TODO this causes flickering

		if path in self._last_selected:
			self._last_selected.remove(path)
		else:
			self._last_selected.append(path)

		self._sync_selection()

	def _handle_row_selected(self,sender):
		self._last_selected=self._selected
		self._selected=self._treeview.get_selection().get_selected_rows()[1]

	def _render_name(self,column:Gtk.TreeViewColumn, cell:Gtk.CellRenderer, treemodel:Gtk.TreeModel, iter:Gtk.TreeIter, user):
		entry = treemodel.get_value(iter, NAME_DATA)
		cell.props.text = entry.name
		cell.props.height=self._translate_row_size(self._determine_icon_size())

	def _render_select(self,column:Gtk.TreeViewColumn, cell:Gtk.CellRenderer, treemodel:Gtk.TreeModel, iter:Gtk.TreeIter, user):
		selected = self._treeview.get_selection().get_selected_rows()
		cell.props.active=treemodel.get_path(iter) in selected[1]
		cell.props.height=self._translate_row_size(self._determine_icon_size())

	def _render_icon(self,column:Gtk.TreeViewColumn, cell:Gtk.CellRenderer, treemodel:Gtk.TreeModel, iter:Gtk.TreeIter, user):
		entry=self._treeview.get_model().get_value(iter, NAME_DATA)
		cell.props.stock_size=self._determine_icon_size()
		cell.set_fixed_size(-1,self._translate_icon_size(self._determine_icon_size()))
		if isinstance(entry,data.Directory):
			cell.props.icon_name="folder"
		else:
			cell.props.icon_name="file"
		cell.props.height=self._translate_row_size(self._determine_icon_size())

	def dismiss_focus(self):
		color=self._treeview.get_style_context().get_background_color(Gtk.StateFlags(Gtk.StateType.NORMAL))
		self._frame.modify_bg(Gtk.StateType.NORMAL,Gdk.Color(color.red*65535,color.green*65535,color.blue*65535))

class DirectoryChain(Gtk.Bin):
	def __init__(self,zoom:int):
		Gtk.Bin.__init__(self)
		self._topbar=Gtk.HBox()
		self._topbar_arrow=Gtk.Button()
		self._topbar_label=Gtk.Label()
		self._topbar_entry=Gtk.Entry()
		self._topbar_button=Gtk.Button()
		self._topbar_center=Gtk.HBox()
		self._topbar_label_container=Gtk.EventBox()
		self._topbar_arrow.props.label="Collapse Chain"
		self._topbar_button.props.label="Close Chain"
		self._topbar_arrow.connect("clicked",self._handle_toggle_collapse)
		self._topbar_button.connect("clicked",self._handle_close)
		self._topbar_label_container.connect("button-press-event",self._handle_edit_path)
		self._topbar_entry.connect("key-press-event",self._handle_edit_path_apply)
		self._topbar_entry.connect("focus-out-event",self._handle_edit_path_defocus)
		self._topbar.pack_start(self._topbar_arrow,False,False,0)
		self._topbar_label_container.add(self._topbar_label)
		self._topbar_center.pack_start(self._topbar_label_container,True,True,0)
		self._topbar.pack_start(self._topbar_center,True,True,2)
		self._topbar.pack_start(self._topbar_button,False,False,0)
		self._topbar.props.border_width=2
		self._vbox=Gtk.VBox()
		self._vbox.pack_start(self._topbar,False,False,0)
		self._scroller=Gtk.ScrolledWindow()
		self.add(self._vbox)
		self._hbox=Gtk.HBox()
		self._scroller.add(self._hbox)
		self._vbox.pack_start(self._scroller,True,True,0)
		self._separator=Gtk.HSeparator()
		self._vbox.pack_start(self._separator,False,False,0)
		self._scroller.props.border_width=2

		self.props.vexpand=True
		self.props.hexpand=True
		self.props.valign=0
		self.props.halign=0

		self._views=[]
		self._dir=None

		self._zoom=zoom
		self.zoom(zoom)

		self.open_directory(None,data.ComputerDirectory())

		self.show_all()

	def _handle_edit_path(self,sender,event):
		if self._topbar_entry.get_parent() is None:
			if isinstance(self._dir,data.FSEntry):
				self._topbar_entry.props.text=str(self._dir)
			else:
				self._topbar_entry.props.text=""
			self._topbar_center.remove(self._topbar_label_container)
			self._topbar_center.pack_start(self._topbar_entry,True,True,0)
			self._topbar_center.show_all()
			self._topbar_entry.grab_focus()

	def _handle_edit_path_apply(self,sender,event):
		if self._topbar_label_container.get_parent() is None and event.keyval==Gdk.KEY_Return:
			if self._topbar_entry.props.text!="":
				self.open_directory(self._dir,data.FSDirectory(self._topbar_entry.props.text))
			self._topbar_center.pack_start(self._topbar_label_container,True,True,0)
			self._topbar_center.remove(self._topbar_entry)
			self._topbar_center.show_all()

	def _handle_edit_path_defocus(self, sender, event):
		if self._topbar_label_container.get_parent() is None:
			self._topbar_center.pack_start(self._topbar_label_container, True, True, 0)
			self._topbar_center.remove(self._topbar_entry)
			self._topbar_center.show_all()

	def zoom(self,zoom:int):
		self._zoom=zoom
		self._scroller.props.height_request=zoom/1.5+50 # this size is about right for the max size to be a 3x2 grid on a 1080p screen
		for view in self._views:
			view.zoom(zoom)

	def open_directory(self,parent:Optional[data.Directory],dir:data.Directory):
		while len(self._views)>0 and (self._views[-1].directory.is_parent(dir)==False or self._views[-1].directory==dir or isinstance(parent,data.ComputerDirectory) and not isinstance(self._views[-1].directory,data.ComputerDirectory)):
			view=self._views.pop()
			self._hbox.remove(view)
			view.destroy()

		new_view=DirectoryView(self,dir,self._zoom)
		self._hbox.pack_start(new_view,False,False,0)
		self._views.append(new_view)
		self._dir=dir
		if isinstance(dir,data.FSEntry):
			self._topbar_label.props.label=str(dir)
		elif isinstance(dir,data.ComputerDirectory):
			self._topbar_label.props.label="My Computer"

	def dismiss_focus(self):
		for view in self._views:
			view.dismiss_focus()

	def deselect_all(self):
		for view in self._views:
			view.deselect_all()

	def _handle_toggle_collapse(self,sender):
		if self._scroller.props.visible:
			self._scroller.hide()
			self.props.vexpand=False
		else:
			self._scroller.show()
			self.props.vexpand=True

	def _handle_close(self,sender):
		MainWindow.instance.remove_chain(self)

class Statusbar(Gtk.Bin):
	def __init__(self):
		Gtk.Bin.__init__(self)
		self._lefttext=Gtk.Label()
		self._righttext=Gtk.Label()
		self._leftbutton=Gtk.Button()
		self._rightbutton1=Gtk.Button()
		self._rightbutton2=Gtk.Button()
		self._hbox=Gtk.HBox()
		self._hbox.pack_start(self._leftbutton,False,False,0)
		self._hbox.pack_start(self._lefttext,True,True,0)
		self._hbox.pack_start(self._righttext,True,True,0)
		self._hbox.pack_start(self._rightbutton1,False,False,0)
		self._hbox.pack_start(self._rightbutton2,False,False,0)
		self.add(self._hbox)

		self._leftbutton.props.label="New Chain"
		self._rightbutton1.props.label="Decrease Zoom"
		self._rightbutton2.props.label="Increase Zoom"
		self._leftbutton.connect("clicked",self._handle_new_chain)
		self._rightbutton1.connect("clicked",self._handle_zoom_out)
		self._rightbutton2.connect("clicked",self._handle_zoom_in)

	def _handle_new_chain(self,sender):
		MainWindow.instance.add_chain()

	def _handle_zoom_out(self,sender):
		MainWindow.instance.zoom_out()
	def _handle_zoom_in(self,sender):
		MainWindow.instance.zoom_in()

class MainWindow(Gtk.Window):
	instance=None
	def __init__(self):
		if MainWindow.instance is not None:
			raise RuntimeError("Midgard is designed to be a single-window file manager and so does not support multiple window instances.")
		MainWindow.instance=self

		Gtk.Window.__init__(self, title="Midgard")
		self.props.width_request=600
		self.props.height_request=400
		self._scroller=Gtk.ScrolledWindow()
		self._vbox = Gtk.VBox()
		self._grid=Gtk.Grid()
		self._scroller.add(self._grid)
		self._vbox.pack_start(self._scroller,True,True,0)
		self._statusbar=Statusbar()
		self._vbox.pack_start(self._statusbar,False,False,0)
		self.add(self._vbox)
		self._statusbar.props.border_width=2

		self._chains=[]
		self._zoom=DEFAULT_ZOOM
		self._focused_view=None

		self.add_chain()

		self.show_all()

	def add_chain(self):
		chain=DirectoryChain(self._zoom)
		length=len(self._chains)
		if length==0:
			self._grid.attach(chain,0,0,1,1)
		else:
			self._grid.attach_next_to(chain,self._chains[-1],Gtk.PositionType.BOTTOM,1,1)
		self._chains.append(chain)

	def remove_chain(self,chain):
		self._chains.remove(chain)
		chain.destroy()

	def zoom_out(self):
		self._zoom-=20
		if self._zoom<200:
			self._zoom=200
		for chain in self._chains:
			chain.zoom(self._zoom)

	def zoom_in(self):
		self._zoom+=20
		if self._zoom>625:
			self._zoom=625
		for chain in self._chains:
			chain.zoom(self._zoom)

	def set_focus(self,view):
		self._focused_view=view
		for chain in self._chains:
			chain.dismiss_focus()