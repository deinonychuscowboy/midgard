import os.path
import pathlib
import platform
from abc import abstractmethod
from typing import Optional

from gi.repository import GObject


class Entry(GObject.Object):
	@abstractmethod
	def _get_name(self)->str: pass
	@abstractmethod
	def _set_name(self, value:str)->None: pass
	name=property(lambda self:self._get_name(),lambda self,value:self._set_name(value))
	@abstractmethod
	def _get_hidden(self)->Optional[bool]: pass
	@abstractmethod
	def _set_hidden(self, value:bool)->None: pass
	hidden=property(lambda self:self._get_hidden(), lambda self,value:self._set_hidden(value))

class Directory(Entry):
	@abstractmethod
	def __iter__(self): pass
	@abstractmethod
	def _get_subdirectories(self)->tuple: pass
	subdirectories = property(lambda self:self._get_subdirectories())
	@abstractmethod
	def _get_files(self)->tuple: pass
	files = property(lambda self:self._get_files())
	@abstractmethod
	def is_parent(self,entry:Entry)->Optional[bool]: pass

class FSEntry(Entry):
	def __init__(self,path:str,special_name:Optional[str]=None):
		super().__init__()
		self._special_name=special_name
		if path.endswith("/") and len(path)>1:
			path=path[:-1]
		self._path=path
		self._hash=hash(path)
	def __eq__(self, other):
		return isinstance(other,FSEntry) and self._path==other._path
	def __hash__(self):
		return self._hash
	def __fspath__(self):
		return self._path
	def __repr__(self):
		return self._path

	def _set_name(self, value: str)->None:
		self._path=os.path.dirname(self._path)+os.path.sep+value

	def _get_name(self)->str:
		return self._special_name if self._special_name is not None else os.path.basename(self._path)
	def _get_hidden(self)->Optional[bool]:
		# TODO linux-specific
		return self.name.startswith(".")
	def _set_hidden(self, value:bool)->None:
		# TODO linux-specific
		if self.hidden and not value:
			self.name=self.name.replace(".","",1)
		elif not self.hidden and value:
			self.name="."+self.name

class FSDirectory(FSEntry,Directory):
	def __init__(self,path:str,special_name:Optional[str]=None):
		super().__init__(path,special_name)

	def __iter__(self):
		return iter(self.subdirectories+self.files)

	def is_parent(self,entry:Entry)->Optional[bool]:
		if isinstance(entry,FSEntry):
			return entry._path.startswith(self._path)
		else:
			return None

	def _get_files(self)->tuple:
		return tuple(FSEntry(os.path.join(self._path,f)) for f in os.listdir(self._path) if os.path.isfile(os.path.join(self._path,f)))

	def _get_subdirectories(self)->tuple:
		return tuple(FSDirectory(os.path.join(self._path,f)) for f in os.listdir(self._path) if os.path.isdir(os.path.join(self._path,f)))

class ComputerDirectory(Directory):
	def __init__(self):
		super().__init__()

	def __fspath__(self):
		return pathlib.Path.root

	def _set_hidden(self, value: bool) -> None:
		raise RuntimeError("Computer cannot be set to hidden.")

	def _get_hidden(self) -> Optional[bool]:
		return False

	def __iter__(self):
		return iter((FSDirectory("/", "Filesystem Root"), FSDirectory(os.path.expanduser("~"), "Home")))

	def _set_name(self, value: str)->None:
		raise RuntimeError("Computer's name cannot be changed via this interface.")

	def _get_name(self)->str:
		return platform.node()

	def is_parent(self,entry:Entry)->Optional[bool]:
		return True

	def _get_files(self)->tuple:
		return ()

	def _get_subdirectories(self)->tuple:
		return self.__iter__()